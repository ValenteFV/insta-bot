absl-py==0.8.1
astor==0.8.0
beautifulsoup4==4.8.1
bs4==0.0.1
cycler==0.10.0
enum34==1.1.6
futures==3.1.1
gast==0.3.2
grpcio==1.25.0
h5py==2.10.0
imageai==2.0.2
Keras==2.3.1
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.0
kiwisolver==1.1.0
Markdown==3.1.1
matplotlib==3.1.2
numpy==1.17.4
opencv-python==4.1.2.30
Pillow==6.2.1
pkg-resources==0.0.0
progress==1.5
protobuf==3.10.0
pyparsing==2.4.5
python-dateutil==2.8.1
PyYAML==5.1.2
scipy==1.3.2
selenium==3.141.0
six==1.13.0
soupsieve==1.9.5
tensorboard==1.12.2
tensorflow==1.12.0
termcolor==1.1.0
urllib3==1.25.7
Werkzeug==0.16.0