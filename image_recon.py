# https://medium.com/@guymodscientist/image-prediction-with-10-lines-of-code-3266f4039c7a
# must be installed in python 3.6
# python3 -m pip install --upgrade https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-1.12.0-py3-none-any.whl
# wget https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5
#
#
#
# sudo apt-get update
# sudo apt-get install -y unzip xvfb libxi6 libgconf-2-4

# sudo apt-get install default-jdk 

# LATEST=$(wget -q -O - http://chromedriver.storage.googleapis.com/LATEST_RELEASE)
# wget http://chromedriver.storage.googleapis.com/$LATEST/chromedriver_linux64.zip
# unzip chromedriver_linux64.zip && sudo ln -s $PWD/chromedriver /usr/local/bin/chromedriver
#
# sudo apt-get install chromium-chromedriver    



from imageai.Prediction import ImagePrediction
import os


def image_recon(filename):
    print ("Running image recon")
    execution_path = os.getcwd()
    prediction = ImagePrediction()
    prediction.setModelTypeAsResNet()
    prediction.setModelPath( execution_path + r"/resnet50_weights_tf_dim_ordering_tf_kernels.h5")
    print (execution_path + r"/resnet50_weights_tf_dim_ordering_tf_kernels.h5")
    prediction.loadModel()


    prediction, percentage_probabilities = prediction.predictImage(filename, result_count=1)
    print(prediction)
    prediction1 = prediction[0]
    f_prediction = prediction1.replace("_", " ")
    not_wanted_list = ["monkey", "gorilla", "chimpanzee"]
    if any(not_wanted_list) == f_prediction:
        f_prediction = "thought"


    return f_prediction


